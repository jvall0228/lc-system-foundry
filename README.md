# Last Chronicle Foundry Game System

## Description

A game system written for the foundry game system. Follows the Pathfinder 1e ruleset with some popular 3rd party material and homebrew content.

## Installation

Use the `npm install` and `npm run build` commands to build the project to the `/dist` folder. Currently the project has not reached a point where it can be built.

## Dev Environment

Use VSCode with the prettier and eslint extensions. Make sure to run `npm install` first.

## Usage

TODO: Insert pictures of use here.
